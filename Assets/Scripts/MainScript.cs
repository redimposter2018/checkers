using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScript : MonoBehaviour
{
    public Image cell;
    //int rowColumn = 1;//���-�� ���������� ��������������� �������
    public Button unit;
    Vector3 mousePos;
    int name1;//��� ������
    bool checkWhite = true;//��������, ���������� �� � ����� ������
    bool checkClick = false;//�������� �������� �� �����
    void Start()
    {
        for(int i = 0; i < 8; i++)//��������� �����
        {
            for(int j = 0; j < 8; j++)//��������� �����
            {
                Image clone = Instantiate(cell);
                clone.transform.SetParent(transform.GetChild(0), false);
                name1++;
                clone.name = name1.ToString();
                if (name1 % 2 == 0)//�������� �� ��������
                {
                    if (checkWhite)
                    {
                        clone.color = new Color(0, 0, 0, 255);
                    }
                    else
                    {
                        Destroy(clone.transform.GetChild(0).gameObject);//����������� �����
                    }
                }
                else
                {
                    if (!checkWhite)
                    {
                        clone.color = new Color(0, 0, 0, 255);
                    }
                    else
                    {
                        Destroy(clone.transform.GetChild(0).gameObject);
                    }
                }
                if (i > 4)
                {
                    clone.transform.GetChild(0).GetComponent<Image>().color = new Color(50, 255, 0, 255);
                }
                if (i > 2 && i < 5)
                {
                    Destroy(clone.transform.GetChild(0).gameObject);
                }
            }
            //rowColumn++;
            checkWhite = !checkWhite;
        }
    }
    void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (checkClick)
        {
            unit.transform.position = new Vector2(mousePos.x, mousePos.y);
        }
    }
    public void onClickUnit()
    {
        checkClick = !checkClick;
    }
}
